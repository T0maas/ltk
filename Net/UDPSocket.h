#pragma once 

namespace ltk {

    class UDPSocket{
        protected:
        int sock;
        public:
        UDPSocket();
        UDPSocket(int skt);
        int getFD();
        void Close();
        void setTimeout(int timeout);
    };
}