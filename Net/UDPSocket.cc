#include "UDPSocket.h"
#include <sys/socket.h>
#include <unistd.h>

using namespace ltk;

UDPSocket::UDPSocket()
{
    sock = socket(AF_INET, SOCK_DGRAM, 0);
}

UDPSocket::UDPSocket(int skt)
{
    this->sock = skt;
}


int UDPSocket::getFD() {
    return this->sock;
}

void UDPSocket::Close() {
    close(sock);
}

void UDPSocket::setTimeout(int time) {
    timeval timeout;      
    timeout.tv_sec = 0;
    timeout.tv_usec = time*1000;
    setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
    setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
}