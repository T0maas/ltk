#pragma once 
#include <exception>
#include <memory>

namespace ltk {

  class ClientDisconnectException: public std::exception 
  {
    public:
    virtual const char* what() const throw()
    {
      return "Client disconnected";
    }
  };

  class SocketErrException: public std::exception 
  {
    public:
    virtual const char* what() const throw()
    {
      return "Socket error";
    }
  };


  class ClientTimeoutException: public std::exception 
  {
    public:
    virtual const char* what() const throw()
    {
      return "Client timeout";
    }
  };


  class TCPSocket{
      protected:
      int sock;
      public:
      TCPSocket();
      TCPSocket(int skt);
      void Listen();
      int getFD();
      void Close();
      void setTimeout(int timeout);
      void Reusable(bool reus=true);
  };

  typedef std::shared_ptr<TCPSocket> TCPSocket_Ref;
}