#include "UDPPeer.h"
#include <string.h>
#include <vector>
#include <iostream>
#include <arpa/inet.h>
#include <thread>
using namespace std;
using namespace ltk;

UDPPeer::UDPPeer() {
    
}

void UDPPeer::Bind(int port, string address)
{
    int length = sizeof(server);
    bzero(&server, length);
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(port);
    bind(skt.getFD(), (struct sockaddr *)&server, length);
}

void UDPPeer::Broadcast(bool b) {
  setsockopt(skt.getFD(), SOL_SOCKET, SO_BROADCAST, &b, sizeof(b));
}

void UDPPeer::Reuse(bool reuseaddr)
{
    setsockopt(skt.getFD(), SOL_SOCKET, SO_REUSEADDR, (char *)&reuseaddr, sizeof(reuseaddr));
    setsockopt(skt.getFD(), SOL_SOCKET, SO_REUSEPORT, (char *)&reuseaddr, sizeof(reuseaddr));
}

UDPPeer::~UDPPeer()
{
    skt.Close();
}

UDPDatagram UDPPeer::Recv()
{
    char buf[UDPBUFSIZE];

    socklen_t fromlen = sizeof(struct sockaddr_in);
    struct sockaddr_in from;
    int n = recvfrom(skt.getFD(), buf, UDPBUFSIZE, 0, (struct sockaddr *)&from, &fromlen);
    if (n >= 0)
    {
        vector<unsigned char> data(buf, buf + n);
        UDPDatagram dg;
        char str[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &(from.sin_addr), str, INET_ADDRSTRLEN);
        dg.address = str;
        dg.port = ntohs(from.sin_port);
        dg.data = std::move(data);
        return dg;
    }
    else
    {
        // cout << errno << " " << strerror(errno) << endl;
        switch (errno)
        {
        case EAGAIN:
            break;

        default:
            break;
        }
        throw std::runtime_error(strerror(errno));
    }
}

void UDPPeer::setTimeout(int time)
{
    skt.setTimeout(time);
}

void UDPPeer::JoinMulticast(std::string group, std::string listen_address)
{
    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr = inet_addr(group.c_str());
    if (listen_address.length() == 0 ) {
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    }
    else 
        mreq.imr_interface.s_addr = inet_addr(listen_address.c_str());
    setsockopt(skt.getFD(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &mreq, sizeof(mreq));
}


void UDPPeer::SendTo(std::string data, std::string addres, int port) {
    vector<char> data2(data.begin(),data.end() );
    SendTo(data2,addres, port);
}

void UDPPeer::SendTo(vector<char> data,  std::string addres, int port ) {
    socklen_t fromlen = sizeof(struct sockaddr_in);
    sockaddr_in from;
    from.sin_family = AF_INET;
    from.sin_port = htons (port);
    inet_pton(AF_INET, addres.c_str(),  &(from.sin_addr) );
    int res = sendto(skt.getFD(),data.data(),data.size(),0,(struct sockaddr *)&from,fromlen);
}

void UDPPeer::StartLoop() {
    thread th([=] {
        this->threadid = pthread_self();
        while (true)
        {
            auto dg = this->Recv();
            this->DataIncoming.Invoke(dg);
        }
        
    });
    th.detach();
}