#include "TCPClient.h"
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <arpa/inet.h>
#include <netinet/tcp.h>
using namespace std;
using namespace ltk;

TCPClient::TCPClient(){
    this->sock = make_shared<TCPSocket>();
}

TCPClient::TCPClient(int sockfd, const TCPServer *srv)  : parent(srv){
    this->sock = make_shared<TCPSocket>(sockfd);
}

std::vector<unsigned char> TCPClient::Read(){
    char buffer[100];
    char q=0;
    int nread = read(sock->getFD(),buffer,100);
    if (nread == 0) {
        throw ClientDisconnectException();
    }


    if (nread == -1) {
     throw ClientDisconnectException();
    }

    vector<unsigned char> out(buffer,buffer+nread);
    int count;
    ioctl(sock->getFD(), FIONREAD, &count);
    if (count > 0) {
        unsigned char buffer2[count];
        int nread = read(sock->getFD(),buffer2,count);
        std::copy(buffer2,buffer2+count,std::back_inserter(out));
    }

    return out;
}

std::string TCPClient::ReadStr(){
    auto data = this->Read();
    std::string msg(data.begin(), data.end());
    return msg;
}

TCPClient::~TCPClient() {

}

void TCPClient::Write(std::vector<unsigned char> data){
    write(sock->getFD(),data.data(),data.size());
}

void TCPClient::Write(std::string message){
    std::vector<unsigned char> data(message.begin(),message.end() );
    this->Write(data);
}


void TCPClient::SetKeepAlive(bool enabled, unsigned int idle,unsigned int interval, unsigned int maxcount ) {
    
    if (setsockopt(sock->getFD(), SOL_SOCKET, SO_KEEPALIVE, &enabled, sizeof(int)) == -1) {
        cout << strerror(errno) <<endl;
        throw SocketErrException();
    }
    if (setsockopt(sock->getFD(), IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(idle)) == -1) {
        cout << strerror(errno) <<endl;
        throw SocketErrException();
    }
    if (setsockopt(sock->getFD(), IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(interval)) == -1) {
        cout << strerror(errno) <<endl;
        throw SocketErrException();
    }
    if (setsockopt(sock->getFD(), IPPROTO_TCP, TCP_KEEPCNT, &maxcount, sizeof(maxcount)) == -1) {
        cout << strerror(errno) <<endl;
        throw SocketErrException();
    }
}




int TCPClient::getFD() {
    return sock->getFD();
}

void TCPClient::StopReadt() 
{
    if (this->threadid != 0)
    {
        pthread_cancel(this->threadid);
        this->readthread->join();
    }
}

void TCPClient::SetTimeout(unsigned int time){
    this->sock->setTimeout(time);
}


std::string TCPClient::getIP(){
    char str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(cli_addr.sin_addr), str, INET_ADDRSTRLEN);
    return str;
}

int TCPClient::getPort(){
    return htons(cli_addr.sin_port);
}

void TCPClient::Connect(std::string address, int port){
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    inet_pton(AF_INET,  address.c_str() , &(serv_addr.sin_addr));
    serv_addr.sin_port = htons(port);
    if (connect(sock->getFD(), (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        throw SocketErrException();
}

void TCPClient::Close(){
    this->sock->Close();
}

void TCPClient::Reusable(bool reus){
    this->sock->Reusable(reus);
}

void TCPClient::ForcePort(int port){
    cli_addr.sin_family = AF_INET;
    cli_addr.sin_port = htons(port);
    cli_addr.sin_addr.s_addr = INADDR_ANY;
    int bindres = bind(sock->getFD(), (sockaddr *)&cli_addr, sizeof(cli_addr));
    if (bindres < 0)
    {
        throw std::runtime_error(strerror(errno));
    }
}

