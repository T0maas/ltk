#pragma once
#include <string>
#include <vector>

namespace ltk {
struct UDPDatagram
{
    std::string address;
    int port;
    std::vector<unsigned char> data;
};
}