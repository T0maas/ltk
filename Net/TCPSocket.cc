#include "TCPSocket.h"
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <netinet/in.h>

using namespace ltk;

TCPSocket::TCPSocket()
{
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

TCPSocket::TCPSocket(int skt)
{
    this->sock = skt;
}


int TCPSocket::getFD() {
    return this->sock;
}

void TCPSocket::Close() {
    close(sock);
}

void TCPSocket::setTimeout(int time) {
    timeval timeout;      
    timeout.tv_sec = time;
    timeout.tv_usec = 0;
    setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
    setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
}

void TCPSocket::Listen() {
   int res =  listen(sock,5);
   if (res == -1) {
       std::cerr << "listen er: " << strerror(errno)<< std::endl ;
   }
}

void TCPSocket::Reusable(bool enable) {
    bool enabled;
    if (enable) {
        enabled = true;
    }
    else {
        enabled = false;
    }
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &enabled, sizeof(int)) < 0)
        throw std::runtime_error(strerror(errno));
    
}