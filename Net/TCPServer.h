#pragma once
#include <sys/types.h> 
#include "TCPSocket.h"
#include <netinet/in.h>
#include <string>
#include "TCPClient.h"
#include "Event.h"
#include <exception>
#include <memory>
#include <Object.h>


namespace ltk {
    typedef std::shared_ptr<TCPClient> TCPClient_Ref;
    class TCPServer : public Object
    {
    private:
        TCPSocket_Ref sock;
        sockaddr_in server;
        pthread_t threadid=0;
        int port;
        TCPClient_Ref _Accept();
        std::vector<TCPClient_Ref> clientlist;

    public:
        Event<TCPClient_Ref> ClientConnected;
        Event<TCPClient_Ref> ClientDisconnected;
        TCPServer(int port, std::string addr = "0.0.0.0",bool reuseaddr = false);
        void Listen();
        TCPClient Accept();
        void SetTimeout(unsigned int time);
        void Reusable(bool reus);
        void StartLoop();
        ~TCPServer();
        int getFD();
    };
}