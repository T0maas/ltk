#pragma once 
#include <netinet/in.h>
#include "TCPSocket.h"
#include <vector>
#include <string>
#include "Event.h"
#include "Object.h"
#include <memory>
#include <thread>


namespace ltk {
    class TCPServer;
    class TCPClient : public Object
    {
        friend class TCPServer;
    private:
        TCPSocket_Ref sock;
        const TCPServer * parent;
        sockaddr_in cli_addr, serv_addr;
        pthread_t threadid=0;
        std::thread * readthread;

    public:
        Event<std::shared_ptr<TCPClient>,std::vector<unsigned char>> DataIncomming;
        TCPClient();
        ~TCPClient();
        TCPClient(int sockfd, const ltk::TCPServer * srv);
        std::vector<unsigned char> Read();
        std::string ReadStr();
        void Write(std::vector<unsigned char> data);
        void Write(std::string message);
        void Listen();
        void SetTimeout(unsigned int time);
        void check_disc();
        std::string getIP();
        int getPort();
        void Connect(std::string address, int port);
        void Close();
        void Reusable(bool reus);
        void ForcePort(int port);
        void SetKeepAlive(bool enabled, unsigned int idle,unsigned int interval, unsigned int maxcount );
        int getFD();
        void StopReadt();
    };
}