#pragma once
#include <netinet/in.h>
#include "UDPSocket.h"
#include "UDPDatagram.h"
#include "Event.h"
#include <string>
#include <vector>
#define UDPBUFSIZE 1024*8


namespace ltk {
class UDPPeer
{
private:
   sockaddr_in server;
   UDPSocket skt;
   pthread_t threadid;

   public:
   UDPPeer();
   ltk::Event<ltk::UDPDatagram> DataIncoming;
   void Bind(int port, std::string address = "0.0.0.0");
   void Reuse(bool reuse);
   void JoinMulticast(std::string group, std::string listen_address = "");
   ~UDPPeer();
   UDPDatagram Recv();
   void setTimeout(int time);
   void SendTo(std::vector<char> data, std::string addres, int port);
   void SendTo(std::string data, std::string addres, int port);
   void Broadcast(bool b);
   void StartLoop();
};

}