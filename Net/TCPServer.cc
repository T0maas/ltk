#include "TCPServer.h"
#include <stdexcept>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>
#include <iostream>
#include <memory>

using namespace ltk;

TCPServer::TCPServer(int port, std::string addr, bool reuseaddr)
{
    this->sock = std::make_shared<TCPSocket>();
    this->port = port;
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(addr.c_str());
    server.sin_port = htons(port);
    Reusable(reuseaddr);
    int bindres = bind(sock->getFD(), (sockaddr *)&server, sizeof(server));
    if (bindres < 0)
    {
        throw std::runtime_error(strerror(errno));
    }
}

void TCPServer::Listen()
{
    sock->Listen();
}

TCPClient_Ref TCPServer::_Accept()
{
    sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int clifd = accept(sock->getFD(), (sockaddr *)&cli_addr, &clilen);
    if (clifd < 0)
    {
        throw std::runtime_error(strerror(errno));
    }

    TCPClient_Ref cl = std::make_shared<TCPClient>(clifd, this);
    cl->cli_addr = cli_addr;
    return cl;
}

TCPClient TCPServer::Accept()
{
    sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int clifd = accept(sock->getFD(), (sockaddr *)&cli_addr, &clilen);
    if (clifd < 0)
    {
        throw std::runtime_error(strerror(errno));
    }

    TCPClient cl(clifd, this);
    cl.cli_addr = cli_addr;
    return cl;
}

int TCPServer::getFD()
{
    return sock->getFD();
}

TCPServer::~TCPServer()
{
    std::cout << "~TCPServer()\n";
    if (this->threadid != 0)
    {
        pthread_cancel(this->threadid);
    }
    for (auto & cl : clientlist) {
        cl->StopReadt();
        delete cl->readthread;
    }
    clientlist.clear();
    sock->Close();
}

void TCPServer::SetTimeout(unsigned int time)
{
    sock->setTimeout(time / 1000);
}

void TCPServer::Reusable(bool enable)
{
    sock->Reusable(enable);
}

void TCPServer::StartLoop()
{
    std::thread th(
        [=]
        {
            this->threadid = pthread_self();
            while (true)
            {
                auto client = this->_Accept();
                this->clientlist.push_back(client);

                std::thread *clreadth = new std::thread(
                    [=]
                    {
                        this->ClientConnected.Invoke(client);
                        std::vector<unsigned char> data;
                        client->threadid = pthread_self();
                        while (true)
                        {
                            try
                            {
                                data = client->Read();
                                client->DataIncomming.Invoke(client, data);
                            }
                            catch (ClientDisconnectException &ex)
                            {
                                this->ClientDisconnected.Invoke(client);
                                client->readthread->detach();
                                delete client->readthread;
                                auto found = std::find_if(clientlist.begin(), clientlist.end(),
                                             [&client](TCPClient_Ref clr)
                                             {
                                                 return clr->getFD() == client->getFD();
                                             });
                                    if (found != clientlist.end()) {
                                        clientlist.erase(found);
                                    }
                                break;
                            }
                            catch (ClientTimeoutException &ex)
                            {
                                //Do nothing
                            }
                        }
                    });
               // clreadth.detach();
                client->readthread = clreadth;
            }
        });
    th.detach();
}