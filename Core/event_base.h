#pragma once
#include "Action.h"
namespace ltk
{
    template <typename... Args>
    class Event
    {
    private:
        Action<Args...> act;

    public:
        void Connect(Action<Args...> _act)
        {
            this->act = _act;
        }

        template <typename _Object>
        void Connect(_Object *_class, void (_Object::*_method)(Args...))
        {
            this->act = Action<Args...>(_class, _method);
        }

        void Invoke(Args... args);
    };
}
