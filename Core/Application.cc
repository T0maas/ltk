#include "Application.h"
#include <exception>
#include "Event.h"
using namespace ltk;

Application *Application::app = nullptr;
Event<> Application::onQuit;

Application::Application()  {
    if (Application::app == nullptr) {
        Application::app = this;
    }
    else {
        throw std::runtime_error("Cannot create multiple instances of Application");
    }
}


void Application::Run() & {
    this->running = true;
    while (this->running)
    {
        std::unique_lock<std::mutex> lk(this->m_cv);
        this->cv.wait(lk, [=]{return (this->myq.size() > 0) /*|| (this->running == false )*/  ;});
        while (this->myq.size() > 0)
        {
            this->m_q.lock();
            auto lfn = this->myq.front();
            this->m_q.unlock();
            lfn();
            this->m_q.lock();
            this->myq.pop();
            this->m_q.unlock();
        }
        }
    

}

void Application::Enqueue(std::function<void()> fn) {
    Application::app->m_q.lock();
    Application::app->myq.push(fn);
    Application::app->m_q.unlock();
    Application::app->cv.notify_one();
}

void Application::Quit() {
    Application::app->running = false;
    Application::onQuit.Invoke();
}