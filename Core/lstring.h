#ifndef STRING_H
#define STRING_H
#include <vector>
#include <iostream>
namespace ltk {
class String {
protected:
  char* data = 0;
  size_t length=0;

public:
  size_t Length();
  String();
  String(const char* input);
  String(const char& input);
  String(const String& input);
  String(const double number);

  ~String();

  //User functions
  std::vector<String> Split(String delim);
  bool Contains(const String& text);
  int WhereIs(const String& text);
  bool EndsWith(const String& text);
  bool BeginsWith(const String& text);

  //Operators
  void operator=(char* input);
  void operator=(const String& input);
  String operator+(const String& input);
  String operator+(const char& input);
  void operator+=(const String& input);
  char& operator[]  (size_t index);
  operator char* ();
  operator  char const* ();
  bool operator==(const String& text);
  bool operator==(const char* text);

};


std::ostream& operator<<(std::ostream& os,  String& obj);
}
#endif //STRING_H
