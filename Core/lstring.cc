#include "lstring.h"
#include <string.h>
#include <assert.h>

using namespace ltk;

String::String() {
    this->length = 0;
}

String::String(const char* input) {
    if (this->length > 0)
        delete[] this->data;
    this->length = strlen(input);
    this->data = new char[this->length + 1];
    memcpy(this->data, input, this->length);
    this->data[this->length] = 0;
}

String::String(const char& input) {
    if (this->length > 0)
        delete[] this->data;
    this->length = 1;

    this->data = new char[2];
    memcpy(this->data, &input, this->length);
    this->data[this->length] = 0;
}

String::String(const String& input) {
    if (this->length > 0)
        delete[] this->data;
    this->length = input.length;
    this->data = new char[this->length + 1 ];
    memcpy(this->data, input.data, this->length);
    this->data[this->length] = 0;
}

String::~String() {
    if (this->length > 0)
        delete[] this->data;
    this->data = 0;
    this->length = 0;
}

size_t String::Length() {
    return this->length;
}

std::vector<String> String::Split(String delim) {
    std::vector<String> out;
    String tmp;
    bool spl;
    for (size_t i = 0; i<this->length; i += delim.length) {
        for (size_t j = 0; j < delim.length; j++) {
            if (i + j < this->length) {
                if (this->data[i + j] == delim[j]) {
                    spl = true;
                    //tmp += "\0";
                } else {
                    spl = false;
                    tmp += this->data[i + j];
                }
            }
        }
        if (spl) {
            out.push_back(tmp);
            tmp = (char*) ("");
        }
    }
    if (tmp.length > 0)
        out.push_back(tmp);
    if (spl)
        out.push_back("\0");
    return out;
}

void String::operator=(char *input) {
    if (this->length > 0) {
        delete[] data;
    }
    this->length = strlen(input);
    if (this->length > 0) {
        this->data = new char[this->length + 1];
        memcpy(this->data, input, this->length);
        this->data[this->length] = 0;
    }
}

String String::operator+(const String& input) {
    char* tmpdata = new char[this->length + input.length + 1];
    for (size_t i = 0; i < this->length; i++)
        tmpdata[i] = this->data[i];
    for (size_t i = this->length; i < (this->length + input.length); i++)
        tmpdata[i] = input.data[i - this->length ];

    tmpdata[this->length + input.length] = 0;
    String str(tmpdata);
    delete[] tmpdata;
    return str;
}

String String::operator+(const char& input) {
    char* tmpdata = new char[this->length + 1 + 1];
    for (size_t i = 0; i < this->length; i++)
        tmpdata[i] = this->data[i];
    for (size_t i = this->length; i < (this->length + 1); i++)
        tmpdata[i] = input;

    tmpdata[this->length + 1] = 0;
    String str(tmpdata);
    delete[] tmpdata;
    return str;
}

void String::operator+=(const String &input) {
    char* tmpdata;
    if (this->length > 0) {
        tmpdata = new char[this->length];
        memcpy(tmpdata, this->data, this->length);
        delete[] this->data;
    }

    this->data = new char[ this->length + input.length + 1];

    if (this->length > 0) {
        memcpy(this->data, tmpdata, this->length);
        memcpy(this->data + (this->length), input.data, input.length);
        this->data[(this->length + input.length)] = 0;
        this->length += input.length;
        delete[] tmpdata;
    } else {
        /* for (size_t i=0;i<input.length;i++) {
           this->data[i] = input.data[i];
         }*/
        memcpy(this->data, input.data, input.length);
        this->data[input.length] = 0;
        this->length = input.length;
    }

}

char& String::operator[](size_t index) {
    assert(index < this->length);
    return this->data[index];
}

String::operator char *() {
    return this->data;
}

String::operator char const *() {
    return this->data;
}

void String::operator=(const String& input) {
    if (this->length > 0)
        delete[] this->data;
    this->data = new char[input.length + 1];
    memcpy(this->data, input.data, input.length);
    this->data[input.length] = 0;
    this->length = input.length;
}

std::ostream& operator<<(std::ostream& os, String& obj) {
    for (size_t i = 0; i < obj.Length(); i++)
        os << obj[i];
    return os;
}

bool String::operator==(const String& input) {
    if (this->length == input.length) {
        for (size_t i = 0; i<this->length; i++)
            if (input.data[i] != this->data[i])
                return false;
    } else return false;
    return true;
}

bool String::operator==(const char* input) {
    return this->operator==(String(input));
}

bool String::Contains(const String& text) {
    size_t score = 0;
    for (size_t i = 0; i < this->length; i++) {
        for (size_t j = 0; j < text.length; j++) {
            if (this->data[i + j] == text.data[j]) {
                score++;
            } else if (score > 0) {
                score = 0;
            }
            if (score == text.length)
                return true;
        }
    }
    return false;
}

int String::WhereIs(const String& text) {
    size_t score = 0;
    for (size_t i = 0; i < this->length; i++) {
        for (size_t j = 0; j < text.length; j++) {
            if (this->data[i + j] == text.data[j]) {
                score++;
            } else if (score > 0) {
                score = 0;
            }
            if (score == text.length)
                return i;
        }
    }
    return -1;
}

bool String::EndsWith(const String &text) {

    if (this->length < text.length)
        return false;
    size_t check = 0;
    size_t pre = this->length - text.length;
    for (size_t i = pre; i < this->length; i++) {
        if (this->data[i] == text.data[i - pre])
            check++;
    }
    if (check == text.length)
        return true;
    else return false;
}

String::String(const double number) {
    if (this->length > 0) {
        delete[] data;
    }
     this->data = new char[32];
     memset(data,0,32);
     sprintf(data,"%lg",number);
     this->length = strlen(data);
}


bool String::BeginsWith(const String& text) {
    if (this->length >= text.length) {
        for (int i=0; i < text.length;i++) {
            if (data[i] == text.data[i]) {
                //ok
            }
            else 
                return false;
            
            return true;
        }
    }
    return false;
}