/*
 * ltimer.cc
 *
 *  Created on: 23. 6. 2020
 *      Author: tom
 */

#include "ltimer.h"
#include <thread>

namespace ltk {

    Timer::Timer(unsigned long period) {
        this->msperiod = period;
        this->status = Timer::TimerState::Stopped;
    }

    void Timer::Start() {
        this->status = Timer::TimerState::Running;
        this->tmr_thr = new std::thread(&Timer::timer_method, this); // @suppress("Symbol is not resolved")
    }

    void Timer::Stop() {
        this->status = Timer::TimerState::Stopped;
        if (this->tmr_thr != NULL) {
            this->tmr_thr->join();
            delete this->tmr_thr;
            this->tmr_thr = 0;
        }
    }

    Timer::~Timer() {
        // TODO Auto-generated destructor stub
        this->Stop();
    }

    void Timer::timer_method() {
        while (this->status == Timer::TimerState::Running) {
            this->tick.Invoke(msperiod);
            std::this_thread::sleep_for(std::chrono::milliseconds(msperiod));
        }
    }

    void Timer::SetPeriod(unsigned long period) {
        this->msperiod = period;
    }
} /* namespace ltk */

