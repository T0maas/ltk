#include "Thread.h"

void ltk::Thread::Stop() {
    if (tid > (pthread_t)0) {
        pthread_cancel(this->tid);
        this->state = Stopped;
    }
}

ltk::Thread::States ltk::Thread::GetState() { return this->state; }

ltk::Thread::~Thread() {
    if (this->th != nullptr) {
        this->th->detach();
        delete this->th;
    }
}

void ltk::Thread::Run() {
    auto lambda = [=] {
        this->tid = pthread_self();
        pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
        this->function();
    };

    this->th = new std::thread(lambda);
    this->state = Running;
}

ltk::Thread::Thread(std::function<void()> fn) {
    this->tid = (pthread_t)0;
    this->th = nullptr;
    this->state = Prepared;
    this->function = fn;
}
