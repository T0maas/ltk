#pragma once

#include <Action.h>
#include <functional>
#include <iostream>
#include <thread>

namespace ltk {

class Thread {
protected:
    std::thread *th;
    pthread_t tid;

    enum States { Prepared, Running, Paused, Stopped };
    States state;
    std::function<void()> function;

public:
    States GetState();

    Thread(std::function<void()> fn);

    template <class TObject>
    Thread(void (TObject::*method)(), TObject* obj) {
        this->function = std::bind(method, obj);
        this->th = nullptr;
        this->state = Prepared;
    }

    void Run();
    void Stop();
    ~Thread();
};
} // namespace ltk
