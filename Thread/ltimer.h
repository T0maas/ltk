/*
 * ltimer.h
 *
 *  Created on: 23. 6. 2020
 *      Author: tom
 */
#include <ltk.h>
#include <thread>
#include <condition_variable>

#ifndef THREADING_LTIMER_H_
#define THREADING_LTIMER_H_

namespace ltk {

class Timer {
public:
	enum TimerState {Stopped,Running};
	Timer(unsigned long msperiod);
	virtual ~Timer();
	void Start();
	void Stop();
	void SetPeriod(unsigned long period);
	bool GetState();
	ltk::Event<int> tick;

protected:
	void timer_method();
	std::thread * tmr_thr;
	std::condition_variable cv;
	bool status;
	unsigned long msperiod;
};

} /* namespace ltk */

#endif /* THREADING_LTIMER_H_ */
