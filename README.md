# Lightweight toolkit
A C++ toolkit using OOP and event based system.

## Included features
* Event system
* Networking
    * TCP client/server - event based recieving

## Other Features (in future releases) [TODO]
* Data types
    * string class
    * image/matrix class
* Netwoking
    * UDP client/server
    * Unix sockets
* Threading
    * Thread class (pausable, stopable)
    * Background Activity
    * Timer
* Multimedia
    * Pulseaudio c++ wrapper

# Requirements
* gcc

# Compiling & Installation
1. ./configure
2. make 
3. sudo make install
