#include <iostream>
#include "TCPClient.h"

using namespace std;


int main(int argc, char const *argv[])
{
    ltk::TCPClient client;
    client.Reusable(true);
    client.ForcePort(30001);
    client.Connect("127.0.0.1", 8080);
    client.Write("hello");
    while (true) {
        cout << client.ReadStr();
    }
    return 0;
}
