#include "Application.h"
#include <iostream>
#include "Action.h"
#include "Event.h"
#include <thread>
#include <unistd.h>
using namespace std;

class Tester
{

public:
    ltk::Event<> ontest;
    ltk::Event<int> ontest2;
    Tester()
    {
        ontest.Connect(this, &Tester::Test);
        ontest2.Connect(this, &Tester::func2);
        std::thread t([&]
                      {
                          this_thread::sleep_for(chrono::milliseconds(1000));
                          ontest.Invoke();
                          for (int i = 0; i < 10; i++)
                          {
                              ontest2.Invoke(i);
                             // cout << (void *)&i << endl;
                              this_thread::sleep_for(chrono::milliseconds(100));
                          }

                          this_thread::sleep_for(chrono::milliseconds(100));
                          ltk::Application::Quit();
                      });
        t.detach();
    }
    void Test()
    {
        cout << "TEster" << endl;
    }
    void func2(int n)
    {
        cout << n << endl;
        this_thread::sleep_for(chrono::milliseconds(200));
    }
};

void on_quit()
{
    cout << "Quitting\n";
    this_thread::sleep_for(chrono::milliseconds(100));
}

int main(int argc, char const *argv[])
{
    ltk::Application app;
    ltk::Application::onQuit.Connect(on_quit);
    Tester t;
    app.Run();
    return 0;
}
