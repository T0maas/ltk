#include "TCPServer.h"
#include "Application.h"
#include <iostream>
using namespace std;

class Main
{
    

private:
    ltk::TCPServer serv;

public:
    Main() : serv(8080) {
        serv.ClientConnected.Connect(this, &Main::OnClientConnect);
        serv.ClientDisconnected.Connect(this, &Main::OnClientDisConnect);
        serv.Reusable(true);
        serv.Listen();
        serv.StartLoop();
    }

    void OnClientConnect(ltk::TCPClient_Ref cl) {
        cout << "Client connected: " << cl->getIP() << ":" << cl->getPort() << endl;
        cl->DataIncomming.Connect(this, &Main::OnClientSend);
    }

    void OnClientDisConnect(ltk::TCPClient_Ref cl) {
        cout << "Client disconnected: " << cl->getIP() << ":" << cl->getPort() << endl;
    }

    void OnClientSend(ltk::TCPClient_Ref cl, std::vector<unsigned char> data) {
        string mesg(data.begin(), data.end());
        cout << "Client " << cl->getIP() << ":" << cl->getPort() << " sent: " << mesg << endl;
        if (mesg == "quit") {
            cout << "Exiting...\n";
            ltk::Application::Quit();
        }
    }
};

void onQuit() {
    cout << "Quit\n";
}

int main() {
    ltk::Application app;
    ltk::Application::onQuit.Connect(onQuit);
    Main m;
    app.Run();
}