#include "UDPPeer.h"
#include <iostream>

using namespace std;

class UDPServer
{
private:
    ltk::UDPPeer server;

public:
    UDPServer()
    {
        server.DataIncoming.Connect(this, &UDPServer::OnData);
        server.Reuse(true);
        server.Bind(8080);
        server.StartLoop();
    }

    void OnData(ltk::UDPDatagram datagram) {
        string msg(datagram.data.begin(),datagram.data.end() );
        cout << "[" << datagram.address << ":" << datagram.port << "] " << msg << endl;
    }
};

int main(int argc, char const *argv[])
{
    ltk::Application app;
    UDPServer srv;
    app.Run();
    return 0;
}
